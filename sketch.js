// Déclarations des variables
let hauteur = 450;
let largeur = 400; // hauteur et largeur du dessin
let h;             // coefficient de hauteur utilisé dans le Noise, dépend de x
let hmin = 10;
let hmax = 80;
let noiseScale = 0.028;  // coefficient appliqué à la position de la souris ou à x dans noiseVal
let noiseVal;  // valeur du noise
let strokevar = 9;  // pour la dernière fonction, variable de stroke de chaque ligne

// tableau des valeurs limites de chaque intervalle
let bornes1 = [largeur/3-40, largeur/3+25, 2*largeur/3-25, 2*largeur/3+40]; 
let bornes2 = [largeur/3-30, largeur/3+25, 2*largeur/3-25, 2*largeur/3+30];
let bornes3 = [largeur/3-50, largeur/3+25, 2*largeur/3-25, 2*largeur/3+50];
let bornes_total = [bornes1, bornes2, bornes3];
let bornes;    // liste choisie aléatoirement parmis bornes 1,2 et 3 pour chaque ligne




// Décommenter la fonction souhaitée dans setup ou draw
function setup() {
  createCanvas(600, 600);
  background(0);
  strokeWeight(2.5);
  //base();
  //moreNoise();
  //increment();
  //saveCanvas('unknown_pleasure_3','png');
}
function draw(){
  //animated();
}






// Reproduction de l'image de départ
// Les fonctions suivantes reprennent le même fonctionnement global
function base(){
  
  for(let y=(height-hauteur)/2; y<height-(height-hauteur)/2; y+=10){
    //on parcourt la hauteur de l'image
    bornes = random(bornes_total); //on choisit les bornes de la ligne au hasard
    
    for (let x=0; x < largeur; x++) {
      // on parcourt la largeur 
      noiseVal = noise((x)*noiseScale,y*noiseScale); // on calcul la valeur du noise pour chaque point de la ligne
      
      // on détermine la valeur du coefficient h (hauteur du noise) en fonction de la position du point sur la ligne
      if(x<bornes[0] || x>=bornes[3]){   // x dans les zones extremes (h minimal)
        h = hmin;
      }
      else if(x<bornes[1]){   // x dans la zone de transition gauche 
        h = map(x,bornes[0],bornes[1],hmin,80);  // on met à l'échelle h en fonction de la position du point
      }
      else if(x<bornes[2]){   // x dans la zone centrale (h maximal)
        h = 80;
      }
      else if(x<bornes[3]){  // x dans la zone de transition droite
        h = map(x,bornes[2],bornes[3],80,hmin);
      }
      stroke(0);
      line(x+102, y-noiseVal*h, x+102, height);  // on trace une ligne noire sous le point pour cacher les lignes precedentes
      stroke(255);
      point(x+100, y-noiseVal*h);   // on trace le point
    }
  }
}

// Reproduction de l'image de départ animée
function animated(){
  stroke(0);
  
  for(let x=0; x<largeur; x++){  // corrige un bug laissant les lignes des frames précedentes à l'écran
   line(x+99,(height-hauteur)/2-noiseVal*h, x+99, 0) ;
  }
  
  for(let y=(height-hauteur)/2; y<hauteur+(height-hauteur)/2; y+=10){
    
    bornes = random(bornes_total);

    for (let x=0; x < largeur; x++) {
      
       noiseVal = noise((mouseX+x)*noiseScale,y*noiseScale); // ici noiseVal dépend de la position en x de la souris
      
      if(x<bornes[0] || x>=bornes[3]){
        h = hmin;
      }
      else if(x<bornes[1]){
        h = map(x,bornes[0],bornes[1],hmin,80);
      }
      else if(x<bornes[2]){
        h = 80;
      }
      else if(x<bornes[3]){
        h = map(x,bornes[2],bornes[3],80,hmin);
      }
      stroke(0);
      line(x+102, y-noiseVal*h, x+102, height);
      point(100,y-noiseVal*h+2);  // correction du même bug que plus haut
      point(100,y-noiseVal*h-2);  // idem
      stroke(255);
      point(x+100, y-noiseVal*h);
    }
  }
}

// Version avec bruit augmenté et changement de couleur
function moreNoise(){
  hmax2 = 130;   // nouvelle valeur maximale de h
  
  textSize(70);  
  fill(255, 0, 0);
  text('JOY DIVISION', 65, 80);   // affichage du nom du groupe derrière le dessin
   
  for(let y=(height-hauteur)/2+30; y<hauteur+(height-hauteur)/2+30; y+=10){

    bornes = random(bornes_total);
    
    for (let x=0; x < largeur; x++) {
      
      noiseVal = noise((x)*noiseScale,y*noiseScale);
      
      if(x<bornes[0] || x>=bornes[3]){
        h = hmin;
      }
      else if(x<bornes[1]){
        h = map(x,bornes[0],bornes[1],hmin,hmax2);
      }
      else if(x<bornes[2]){
        h = hmax2;
      } 
      else if(x<bornes[3]){
        h = map(x,bornes[2],bornes[3],hmax2,hmin);
      }
      stroke(0);
      line(x+102, y-noiseVal*h, x, height);
      strokeWeight(1.5);
      stroke(255, 0, 0);
      point(x+100, y-noiseVal*h);
    }
  }
}

// Version modifiée
 function increment(){
    
    background(255);
    textSize(70);
    fill(0, 0, 0);
    text('JOY DIVISION', 65, 80);
   
    for(let y=(height-hauteur)/2+30; y<hauteur+(height-hauteur)/2+30; y+=10){
      bornes = random(bornes_total);  // on choisit au hasard les bornes de la ligne
      let rand = random(5);  // on définit deux valeurs au hasard qui vont modifier la position de nos bornes
      let rand2 = random(5);
      strokevar= strokevar-0.2;  // la variable définnissant l'épaisseur de la ligne baisse pour chaque ligne
      
      strokeWeight(strokevar);
      
      if(bornes[2]>bornes[1]-100){
        // tant que les bornes centrales sont dans le bon ordre, on modifie les valeurs de chacunes de nos bornes
        // pour rester centré, les bornes intérieurs et extérieurs sont modifiées de la même manière à gauche et à droite
        bornes[0]=bornes[0]+rand;
        bornes[3]=bornes[3]-rand;
        bornes[1]=bornes[1]+rand2;
        bornes[2]=bornes[2]-rand2;
        hmax=hmax-1.3;  // la hauteur maximale du noise baisse à chaque ligne
      } 
      else {
        // si nos bornes sont inversées à forces d'être modifiées, le noise est le même sur toute la ligne
        hmax=hmin;
      }

      
      for (let x=0; x < largeur; x++) {

        noiseVal = noise((x)*noiseScale,y*noiseScale);

        if(x<bornes[0] || x>=bornes[3]){
          h = hmin;
        }
        else if(x<bornes[1]){
          h = map(x,bornes[0],bornes[1],hmin,hmax);
        }
        else if(x<bornes[2]){
          h = hmax;
        }
        else if(x<bornes[3]){
          h = map(x,bornes[2],bornes[3],hmax,hmin);
        }
        stroke(255);
        line(x+102, y-noiseVal*h, x+102, height);
        stroke(0, 0, 0);
        point(x+100, y-noiseVal*h);
      }
    }
}


